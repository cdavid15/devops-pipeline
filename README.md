# Proposed DevOps pipeline 

Stub project to illustrate a potential DevOps (or DevSecOps if you wan't to explicity call out the security pieces) Pipeline incorporating the following:

  - creation of docker images for each environment (Development, QA and Production)
  - execution of unit tests
  - static code analysis (via linter tools)
  - code quality analysis (including security analysis) via SonarQube
  - automatic deployment to development environment
    - execution of a set of targetted e2e tests specific to the changes being added (including a smoke test) via Cypress
    - execution of a set of security unit tests via Gauntlt
  - manual deployment to QA Environment
    - execution of e2e regression tests against electron browser via Cypress
    - execution of e2e regression tests against chrome browser via Cypress
    - execution of performance load test via k6
    - manual execution of performance stress test via k6
    - manual execution of performance soak test via k6
    - manual execution of performance spike test via k6
    - execution of Nessus Security Scan
  - pubish production docker image to Nexus Repository
  - Deploy to Production
  

The jobs defined in the `.gitlab-ci.yml` simply echo statements to the console as the main purpose was to help visualise what a poential pipeline may look like.

Some of these jobs may not be required (such as the deployment jobs) however they are required actions and should if possible be incorporated into a CI / CD pipeline and externalised only if absolutely necessary.